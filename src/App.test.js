import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const timeButtonElement = screen.getByText(/Make time request/i);
  expect(timeButtonElement).toBeInTheDocument();
});
