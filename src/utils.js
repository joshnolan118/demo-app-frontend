export function getDifferenceInTime(localTime, serverTime) {
    // input is epoch time in ms
    var differenceInSeconds = Math.abs(localTime - serverTime) / 1000;

    var differenceDate = new Date(differenceInSeconds * 1000);

    var hours = differenceDate.getUTCHours();
    var minutes = differenceDate.getUTCMinutes();
    var seconds = differenceDate.getUTCSeconds();

    if (hours < 10) {hours = "0" + hours;}
    if (minutes < 10) {minutes = "0" + minutes;}
    if (seconds < 10) {seconds = "0" + seconds;}

    return hours + ':' + minutes + ':' + seconds;
}
