import './App.css';
import { TimeView } from './TimeView';
import React, { useState, useEffect } from 'react';

function App() {
  const [serverTime, setTime] = useState(null);
  const [metrics, setMetrics] = useState(null);

  const [timeRequestInProgress, setTimeProgress] = useState(false);
  const [metricsRequestInProgress, setMetricsProgress] = useState(false);

  function makeTimeRequest() {
    setTimeProgress(true);
    // wrap in a set timeout because the API request returns too quickly to see the bg change :)
    setTimeout(() => {
        fetch("https://jn-demo-app.herokuapp.com/time", {
        headers: new Headers({ 'Authorization': 'mysecrettoken' })
      })
      .then(response => response.json())
      .then(json => setTime(json.epoch))
      .then(setTimeout(1000))
      .then(setTimeProgress(false))
      .catch(error => console.error(error));
    }, 2000);
    
  }

  function makeMetricsRequest() {
    setMetricsProgress(true);

    setTimeout(() => {
      fetch("https://jn-demo-app.herokuapp.com/metrics", {
        headers: new Headers({ 'Authorization': 'mysecrettoken' })
      })
      .then(response => response.text())
      .then(text => setMetrics(text))
      .then(setMetricsProgress(false))
      .catch(error => console.error(error));
    }, 2000);
  }

  useEffect(() => {
    // firest request
    makeTimeRequest();
    makeMetricsRequest();
    
    // subsequest requests at 30s intervals
    setInterval(() => {
        makeTimeRequest();
        makeMetricsRequest();
      }, 30*1000);
    }, []
  );


  return (
    <div className="App">
      <header className="App-header">
        <h1>Hello World</h1>
      </header>
      <div className="sections">
        <section className={timeRequestInProgress ? 'section-inprogress' : ''}>
          <button onClick={makeTimeRequest}>Make time request</button>
          <TimeView serverTime={serverTime}/>
        </section>
        <section className={metricsRequestInProgress ? 'section-inprogress' : ''}>
          <button onClick={makeMetricsRequest}>Make metrics request</button>
          <pre className="metricsBlock">{metrics}</pre>
        </section>
      </div>
    </div>
  );
}

export default App;
