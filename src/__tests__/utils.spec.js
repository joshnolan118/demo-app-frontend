import { getDifferenceInTime } from '../utils.js';

describe("getDifferenceInTime method", () => {
    test("it calculates normal inputs correctly", () => {
      
      const baseDate = new Date('May 28, 2023 12:00:00');

      const testCases = [
        { date: new Date('May 28, 2023 12:00:00'), expected: "00:00:00"},
        { date: new Date('May 28, 2023 12:10:00'), expected: "00:10:00"},
        { date: new Date('May 28, 2023 13:00:00'), expected: "01:00:00"},
        { date: new Date('May 28, 2023 13:30:00'), expected: "01:30:00"},
        { date: new Date('May 28, 2023 13:30:30'), expected: "01:30:30"},
        { date: new Date('May 28, 2023 23:59:00'), expected: "11:59:00"},
        { date: new Date('May 29, 2023 00:00:00'), expected: "12:00:00"},
      ];

      for (const test of testCases) {
        expect(getDifferenceInTime(baseDate.getTime(), test.date.getTime())).toEqual(test.expected);
      }
    });
  });