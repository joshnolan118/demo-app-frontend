import React, { Component } from 'react';
import { getDifferenceInTime } from './utils.js';


export class TimeView extends Component {

    timerInterval = 0;

    constructor(props) {
        super(props);
        this.state = { clientDifference: "" };
      }

    componentDidMount() {
        this.timerInterval = setInterval(() => {
            const state = { clientDifference: this.getClientDiff()};
            this.setState(state);
        }, 1000);
    }

    mponentWillUnmount() {
        if (this.timerInterval) {
            clearInterval(this.timerInterval);
            this.timerInterval = null;
        }
    }

    getClientDiff() {
        return getDifferenceInTime(Date.now(), this.props.serverTime);
    }
    
    render() {
      return <div>
                <h3>Server Epoch</h3>
                <h1>{this.props.serverTime}</h1>
                <h3>Client diff</h3>
                <h1>{this.state.clientDifference}</h1>
            </div>;
    }
  }