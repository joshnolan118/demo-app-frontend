# demo-app-frontend
Hello World! This is a react frontend demo app. It calls another demo API to get the server time and server metrics. 

## How to debug
To start the project run the following command:

`npm start`

To test:

`npm test`

## CORS issue
I could not get the API running on localhost, the client requests were blocked due to CORS issues. The API deployed to heroku does not have these issues.